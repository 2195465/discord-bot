import discord
from discord.ext import commands
from discord.voice_client import VoiceClient
import youtube_dl
cmd = commands.Bot(command_prefix='.')
cmd.remove_command("help")
botKEY = 'ODE0NDk2NzE0MDA2MDY5MjYw.YDetLQ.LHMvTc7cXYITkAYWDEWs0LAFKjY'

## Install FFMPEG and YTDL to use this code
# youtube_dl.utils.bug_reports_message = lambda: ''
#
# ytdl_format_options = {
#     'format': 'bestaudio/best',
#     'outtmpl': '%(extractor)s-%(id)s-%(title)s.%(ext)s',
#     'restrictfilenames': True,
#     'noplaylist': True,
#     'nocheckcertificate': True,
#     'ignoreerrors': False,
#     'logtostderr': False,
#     'quiet': True,
#     'no_warnings': True,
#     'default_search': 'auto',
#     'source_address': '0.0.0.0' # bind to ipv4 since ipv6 addresses cause issues sometimes
# }
#
# ffmpeg_options = {
#     'options': '-vn'
# }
#
# ytdl = youtube_dl.YoutubeDL(ytdl_format_options)
#
# class YTDLSource(discord.PCMVolumeTransformer):
#     def __init__(self, source, *, data, volume=0.5):
#         super().__init__(source, volume)
#
#         self.data = data
#
#         self.title = data.get('title')
#         self.url = data.get('url')
#
#     @classmethod
#     async def from_url(cls, url, *, loop=None, stream=False):
#         loop = loop or asyncio.get_event_loop()
#         data = await loop.run_in_executor(None, lambda: ytdl.extract_info(url, download=not stream))
#
#         if 'entries' in data:
#             # take first item from a playlist
#             data = data['entries'][0]
#
#         filename = data['url'] if stream else ytdl.prepare_filename(data)
#         return cls(discord.FFmpegPCMAudio(filename, **ffmpeg_options), data=data)


# WordSets and Images
restricted_words = ["bading","bakla","bobo","tanga","tanginamo", "tangina", "tngina", "vovo", "weak", "cheat", "gago", "qaqo", "futa"]
bronya_sad = "https://cdn.discordapp.com/attachments/813722820462379028/814524582933299200/23f0c2ef3028c41e24c14c28bd3ac931.jpg"
pokes = "https://cdn.discordapp.com/attachments/813722820462379028/814578207658606643/PngItem_5363296.png"
bronya_whisper = "https://cdn.discordapp.com/attachments/813722820462379028/814516730822393896/Satisfied.jpg"
bronya_icon = "https://cdn.discordapp.com/attachments/813722820462379028/814508353266778172/67827486_446446895965930_5358216489126068224_n.png"


@cmd.event
async def on_ready():
    await cmd.change_presence(status=discord.Status.online,activity=discord.Game("Maintenance"))
    print('Bronya is Online')

## Bronya Commands
@cmd.command(name = 'hello', help = 'Bronya will greet you back!')
async def hello(ctx):
    await ctx.send("Hi Captain " + str(ctx.author) + "!")



@cmd.command(name = 'poke', help = 'Bronya will poke the selected user!')
async def poke(ctx, username: discord.Member):
    channel = await username.create_dm()
    await channel.send(str(ctx.author) + " pokes you")
    await channel.send(pokes)



@cmd.command(name='ping', help='Bronya will display its latency!')
async def ping(ctx):
    await ctx.send(f'Bronya Latency: {round(cmd.latency * 1000)}ms')



@cmd.command('play', help = 'Bronya will play music!')
async def play(ctx, url):
    if not ctx.message.author.voice:
        await ctx.send('Captain you are not connected to a voice channel')
    else:
        channel = ctx.message.author.voice.channel
    await channel.connect()



@cmd.command('stop', help = 'Bronya will stop playing music!')
async def stop(ctx):
    voice_client = ctx.message.guild.voice_client
    await voice_client.disconnect()
    await ctx.send('Roger Captain : The music has stopped.')



@cmd.command('clear', help = 'Bronya will delete the number of message tabs')
async def clear(ctx, amount=3):
    await ctx.channel.purge(limit=amount)



@cmd.command('confess', help = 'Bronya will post your anonymous confession')
async def confess(ctx,*,messages):
    channel = cmd.get_channel(824660572644704336)
    embed = discord.Embed(
        title="Anonymous Confession",
        url="https://Bronya-Bot.rafaellachica.repl.co",
        description=messages,
        color=discord.Color.purple())
    embed.set_author(name="Anonymous User!", url="https://Bronya-Bot.rafaellachica.repl.co",icon_url=bronya_icon)
    embed.set_thumbnail(url=bronya_icon)
    embed.set_footer(text="Learn more by typing #help")
    await channel.send(embed=embed)

@cmd.command(pass_context=True)
async def help(ctx):
    embed = discord.Embed(
        title="Bronya Bot Commands",
        url="https://Bronya-Bot.rafaellachica.repl.co",
        description="All Available Commands",
        color=discord.Color.purple())
    embed.set_author(name="Sebastian Lachicus", url="https://Bronya-Bot.rafaellachica.repl.co", icon_url=bronya_icon)
    embed.set_thumbnail(url=bronya_icon)
    embed.add_field(name=".help", value="Returns a list of all commands", inline=False)
    embed.add_field(name=".hello", value="Bronya will greet you back", inline=False)
    embed.add_field(name=".poke", value="<.poke @user> Bronya will send a poking direct message", inline=False)
    embed.add_field(name=".ping", value="Bronya will return its internet connection latency", inline=False)
    embed.add_field(name=".play", value="<.play url>Bronya will join and play the selected music", inline=False)
    embed.add_field(name=".stop", value="Bronya will leave and stop playing music", inline=False)
    embed.add_field(name=".clear", value="<.clear number> will delete the number of messages starting from the latest", inline=False)
    embed.add_field(name=".confess", value="<.confess message>Your message will be posted anonymously in the confession channel", inline=False)
    embed.add_field(name=".learnjp", value="Bronya will display Japanese learning options!", inline=False)
    embed.set_footer(text="Learn more by typing .help")
    await ctx.send(embed=embed)


    ## JAPANESE LEARNING SECTION

    ## JP MEDIA LIBRARY
jap_logo = "https://cdn.discordapp.com/attachments/813722820462379028/824963649470070785/flag.PNG"

hiragana_A = "https://cdn.discordapp.com/attachments/824977447161692161/824980032912228392/AIUEO.PNG"
hiragana_K = "https://cdn.discordapp.com/attachments/824977447161692161/824977480028389386/KAKIKUKEKO.PNG"
hiragana_S = "https://cdn.discordapp.com/attachments/824977447161692161/824977487229485106/SASHISUSESO.PNG"
hiragana_T = "https://cdn.discordapp.com/attachments/824977447161692161/824977488513204244/TACHITSUTETO.PNG"
hiragana_N = "https://cdn.discordapp.com/attachments/824977447161692161/824977484415369266/NANINUNENO.PNG"
hiragana_H = "https://cdn.discordapp.com/attachments/824977447161692161/824977476349591592/HAHIHUHEHO.PNG"
hiragana_M = "https://cdn.discordapp.com/attachments/824977447161692161/824977481576349756/MAMIMUMEMO.PNG"
hiragana_Y = "https://cdn.discordapp.com/attachments/824977447161692161/824977491286425670/YAYUYO.PNG"
hiragana_R = "https://cdn.discordapp.com/attachments/824977447161692161/824977485572734996/RARIRURERO.PNG"
hiragana_W = "https://cdn.discordapp.com/attachments/824977447161692161/824977489617092648/WAO.PNG"
hiragana_n = "https://cdn.discordapp.com/attachments/824977447161692161/824977482644979732/n.PNG"

katakana_A = "https://cdn.discordapp.com/attachments/824977447161692161/824992930644492288/AIUEO.PNG"
katakana_K = "https://cdn.discordapp.com/attachments/824977447161692161/824992934877069352/KAKIKUKEKO.PNG"
katakana_S = "https://cdn.discordapp.com/attachments/824977447161692161/824992944460791828/SASHISUSESO.PNG"
katakana_T = "https://cdn.discordapp.com/attachments/824977447161692161/824992946834243636/TACHITSUTETO.PNG"
katakana_N = "https://cdn.discordapp.com/attachments/824977447161692161/824992940127682560/NANINUNENO.PNG"
katakana_H = "https://cdn.discordapp.com/attachments/824977447161692161/824992932842700810/HAHIHUHEHO.PNG"
katakana_M = "https://cdn.discordapp.com/attachments/824977447161692161/824992936281505843/MAMIMUMEMO.PNG"
katakana_Y = "https://cdn.discordapp.com/attachments/824977447161692161/824993854926749726/YAYUYO.PNG"
katakana_R = "https://cdn.discordapp.com/attachments/824977447161692161/824992942254456852/RARIRURERO.PNG"
katakana_W = "https://cdn.discordapp.com/attachments/824977447161692161/824992948240121896/WAO.PNG"
katakana_n = "https://cdn.discordapp.com/attachments/824977447161692161/824992937791455252/n.PNG"

#Podcasts


@cmd.command()
async def learnjp(ctx):
    embed = discord.Embed(
        title="Learn Japanese Class",
        url="https://Bronya-Bot.rafaellachica.repl.co",
        description="All japanese Learning Commands",
        color=discord.Color.red())
    embed.set_author(name="Bronya Sensei", url="https://Bronya-Bot.rafaellachica.repl.co", icon_url=bronya_icon)
    embed.set_thumbnail(url=jap_logo)
    embed.add_field(name="HIRAGANA COMMANDS", value="list of hiragana commands", inline=False, )
    embed.add_field(name="`.learnHiragana<CHARACTER>`",
                    value="display all hiragana characters [A, K, S, T, N, H, M, Y, R, W, n]", inline=False, )
    embed.add_field(name="`.quizHiragana`", value="asks 10 random hiragana characters", inline=False)
    embed.add_field(name="KATAKANA COMMANDS", value="list of katakana commands", inline=False)
    embed.add_field(name="`.learnkatana`", value="display all katakana characters", inline=False)
    embed.add_field(name="`.quizkatakana`", value="asks 10 random katakana characters", inline=False)
    embed.add_field(name="KANJI COMMANDS", value="list of kanji commands", inline=False)
    embed.add_field(name="`.spawnkanji`", value="will give random kanji character and description", inline=False)
    embed.add_field(name="JP-PODCAST COMMANDS", value="list of podcast commands", inline=False)
    embed.add_field(name="`.playjp<lesson>`",
                    value="will play japanese podcast from [ lesson01, lesson02, lesson03, lesson04, lesson05 ]",
                    inline=False)
    embed.add_field(name="`.pausejp`", value="will pause the japanese podcast", inline=False)
    embed.add_field(name="`.resumejp`", value="will resume the paused japanese podcast", inline=False)
    embed.add_field(name="`.stopjp`", value="will stop playing japanese podcast and bronya will leave", inline=False)

    embed.set_footer(text="Learn more by typing #learnjp")
    await ctx.send(embed=embed)


@cmd.command()
async def learnhiragana(ctx, args):
    channel = cmd.get_channel(824960250158252042)
    arg = args.lower()
    if (arg == "a"):
        await channel.send(hiragana_A)
    if (arg == "k"):
        await channel.send(hiragana_K)
    if (arg == "s"):
        await channel.send(hiragana_S)
    if (arg == "t"):
        await channel.send(hiragana_T)
    if (arg == "n"):
        await channel.send(hiragana_N)
        await channel.send(hiragana_n)
    if (arg == "h"):
        await channel.send(hiragana_H)
    if (arg == "m"):
        await channel.send(hiragana_M)
    if (arg == "y"):
        await channel.send(hiragana_Y)
    if (arg == "r"):
        await channel.send(hiragana_R)
    if (arg == "w"):
        await channel.send(hiragana_W)


@cmd.command()
async def learnkatakana(ctx, args):
    channel = cmd.get_channel(824960473871024139)
    arg = args.lower()
    if (arg == "a"):
        await channel.send(katakana_A)
    if (arg == "k"):
        await channel.send(katakana_K)
    if (arg == "s"):
        await channel.send(katakana_S)
    if (arg == "t"):
        await channel.send(katakana_T)
    if (arg == "n"):
        await channel.send(katakana_N)
        await channel.send(katakana_n)
    if (arg == "h"):
        await channel.send(katakana_H)
    if (arg == "m"):
        await channel.send(katakana_M)
    if (arg == "y"):
        await channel.send(katakana_Y)
    if (arg == "r"):
        await channel.send(katakana_R)
    if (arg == "w"):
        await channel.send(katakana_W)

## Install FFMPEG and YTDL to use this code
# @cmd.command()
# async def playjp(ctx, arg):
#     topic = arg.lower()
#
#     if not ctx.message.author.voice:
#         await ctx.send('`Captain you are not connected to a voice channel`')
#     else:
#         channel = ctx.message.author.voice.channel
#     await channel.connect()
#
#     server = ctx.message.guild
#     voice_channel = server.voice_client
#
#     if topic == "lesson01":
#         async with ctx.typing():
#             player = await YTDLSource.from_url(lesson01, loop=cmd.loop)
#         voice_channel.play(player, after=lambda e: print('Player error: %s' % e) if e else None)
#         await ctx.send('`Now playing: {}`'.format(player.title))
#
#     if topic == "lesson02":
#         async with ctx.typing():
#             player = await YTDLSource.from_url(lesson02, loop=cmd.loop)
#         voice_channel.play(player, after=lambda e: print('Player error: %s' % e) if e else None)
#         await ctx.send('`Now playing: {}`'.format(player.title))
#
#     if topic == "lesson03":
#         async with ctx.typing():
#             player = await YTDLSource.from_url(lesson03, loop=cmd.loop)
#         voice_channel.play(player, after=lambda e: print('Player error: %s' % e) if e else None)
#         await ctx.send('`Now playing: {}`'.format(player.title))
#
#     if topic == "lesson04":
#         async with ctx.typing():
#             player = await YTDLSource.from_url(lesson04, loop=cmd.loop)
#         voice_channel.play(player, after=lambda e: print('Player error: %s' % e) if e else None)
#         await ctx.send('`Now playing: {}`'.format(player.title))
#
#     if topic == "lesson05":
#         async with ctx.typing():
#             player = await YTDLSource.from_url(lesson05, loop=cmd.loop)
#         voice_channel.play(player, after=lambda e: print('Player error: %s' % e) if e else None)
#         await ctx.send('`Now playing: {}`'.format(player.title))
#
#     if not (topic == "lesson01"):
#         if not (topic == "lesson02"):
#             if not (topic == "lesson03"):
#                 if not (topic == "lesson04"):
#                     if not (topic == "lesson05"):
#                         voice_client = ctx.message.guild.voice_client
#         await voice_client.disconnect()
#         await ctx.send('Captain There is no `' + arg + '` in the database')


@cmd.command()
async def pausejp(ctx):
    server = ctx.message.guild
    voice_channel = server.voice_client
    voice_channel.pause()
    await ctx.send('`Roger Captain : The Podcast is Paused.`')


@cmd.command()
async def resumejp(ctx):
    server = ctx.message.guild
    voice_channel = server.voice_client
    voice_channel.resume()
    await ctx.send('`Roger Captain : The Podcast is Resumed.`')


@cmd.command('stopjp', help='Bronya will stop playing music!')
async def stopjp(ctx):
    voice_client = ctx.message.guild.voice_client
    await voice_client.disconnect()
    await ctx.send('`Roger Captain : The Podcast has stopped.`')


## Message Reader
@cmd.event
async def on_message(message):
    sentence = message.content.lower()

    if message.author == cmd.user:
        return

    if any(word in sentence for word in restricted_words):
        await message.channel.send(str(message.author) + ' Bronya is disappointed in your choice of words')
        await message.channel.send(bronya_sad)

    if message.content.startswith('#'):
        await message.channel.send(
            'The `#`prefix has been changed to `.` Try using `.help` to see the list of commands')

    await cmd.process_commands(message)


keep_alive()
cmd.run(botKEY)
